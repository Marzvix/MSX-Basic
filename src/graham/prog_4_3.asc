10 REM *
20 REM * PROG 4.3 - Factorials
30 REM *
40 CLS
50 SUM = 1
60 INPUT "Value "; N
70 FOR I = 1 TO N
80 SUM = SUM * I
90 NEXT
100 PRINT
110 PRINT N;"! = "; SUM
120 END
